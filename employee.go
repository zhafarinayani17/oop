package employee

import "fmt"

type Employee struct { //public
	id     int
	name   string
	salary float32
}

func (self *Employee) raiseSalary(bonus float32) { //private
	self.salary += bonus
}

func (self Employee) format() string { //private
	return fmt.Sprintf("Id: %d, Name: %s, Salary: %.2f", self.id, self.name, self.salary)
}

func (self Employee) Greeting() { // public
	fmt.Sprintf("Hello: %s", self.name)
}
