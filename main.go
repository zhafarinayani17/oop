package main

import (
	f "fmt"
	s "strconv"
)

// 1. Manusia pada umumnya
type Manusia struct {
	nama                     string
	umur                     int
	telepon, hanphone, email string
}

// 2. Manusia berstatus Mahasiswa
type Mahasiswa struct {
	Manusia       // Embed Manusia / semacam extend di OOP Java
	kampus, kosan string
	uang_jajan    float32
}

// 3. Mahasiswa berstatus Karyawan
type Karyawan struct {
	Manusia            // Embed Manusia / semacam extend di OOP Java
	perusahaan, asrama string
	salary             float32
}

// Semua manusia pasti makan
func (m Manusia) makan(makanan string) {
	f.Printf("Makan: %s \n", makanan)
}

// Semua manusia pasti minum
func (m Manusia) minum(minuman string) {
	f.Printf("Minum: %s \n", minuman)
}

// Semua mahasiswa pasti ujian
func (m Mahasiswa) ujian(mata_kuliah string) {
	f.Printf("Ujian mata kuliah: %s \n", mata_kuliah)
}

// Semua karyawan pasti gajian
func (k Karyawan) gajian() {
	f.Printf("Gajian: $.%e ", k.salary)
}

// Interfaces untuk yang pelajar
type Pelajar interface {
	makan(makanan string)
	minum(minuman string)
	ujian(mata_kuliah string)
}

type Pekerja interface {
	makan(makanan string)
	minum(minuman string)
	gajian()
}

// Fungsi Di luar interface / ga ada di interface
func (m *Manusia) biodata() {
	f.Printf(m.nama + " Pengumuman ")
}

// Main function
func main() {
	f.Println(`Ini adalah Data - Data`)
	f.Println("")

	// Implementasi Struct di atas adalah sebagai berikut

	// 1. Mahasiswa
	alyani := Mahasiswa{Manusia{"Alyani", 1, "089655085916", "089655085916", "zhafarinayani17.com"}, "Politeknik Negeri Jakarta", "Kosan ibu Nova", 1000000}
	var alyani_mahasiswa Pelajar // Interface
	f.Println(`Nama:` + alyani.nama)
	f.Println(`Nama:` + s.Itoa(alyani.umur))
	f.Println(`Nama:` + alyani.telepon)
	f.Println(`Nama:` + alyani.hanphone)
	f.Println(`Nama:` + alyani.email)

	alyani_mahasiswa = alyani
	alyani_mahasiswa.makan(`Nasi padang`)
	alyani_mahasiswa.minum(`Es Campur`)
	alyani_mahasiswa.ujian(`Teknik Informatika`)
	// Kalo tidak di komen fungsi di bawah akan ada error: alyani_mahasiswa.biodata undefined (type Pelajar has no field or method biodata)
	// alyani_mahasiswa.biodata() // Ya iya lah masa Mahasiswa bikin Hoax, bego di piara mah ga usah kuliah...
	alyani.biodata() //alyani sebagai manusia bisa khilaf, jadi ini akan jalan

	f.Println(``)
	f.Println(`------------------------`)
	f.Println(``)

	// 2. Karyawan
	nova := Karyawan{Manusia{"nova Mutiara", 27, "087883170167", "087883170167", "mutiara@gmail.com"}, "PT. Cantika", "Rumah dinas Cantika", 20000}
	var nova_karyawan Pekerja // Interface
	f.Println(`Nama:` + nova.nama)
	f.Println(`Nama:` + s.Itoa(nova.umur))
	f.Println(`Nama:` + nova.telepon)
	f.Println(`Nama:` + nova.hanphone)
	f.Println(`Nama:` + nova.email)

	nova_karyawan = nova
	nova_karyawan.makan(`Steak `)
	nova_karyawan.minum(`Es Jeruk`)
	nova_karyawan.gajian()

	f.Println(``)
	// Kalo tidak di komen fungsi di bawah akan ada error: nova_karyawan.biodata undefined (type Pelajar has no field or method biodata)
	// nova_karyawan.biodata() // masa Professional bikin Hoax...
	nova.biodata() //nova sebagai manusia bisa khilaf, jadi ini jalan
	f.Println(``)
	f.Println(`----------------------------`)
	f.Println(``)
	// Manusia biasa
	manusia := Manusia{"rafii", 27, "08534337484", "08534337484", "rafii@gmail.com"}
	f.Println(`Nama:` + manusia.nama)
	f.Println(`Nama:` + s.Itoa(manusia.umur))
	f.Println(`Nama:` + manusia.telepon)
	f.Println(`Nama:` + manusia.hanphone)
	f.Println(`Nama:` + manusia.email)
	manusia.biodata() // Wajar namanya manusia
}

